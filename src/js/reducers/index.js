import { combineReducers } from "redux"

import news from "./newsReducer"
import sportsNews from './sportsNewsReducer'
import businessNews from './businessNewsReducer'

export default combineReducers({
  news,
  sportsNews,
  businessNews
})
