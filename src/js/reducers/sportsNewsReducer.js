export default function reducer(state={
    sportsNews: [],
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "FETCH_SPORTS_NEWS": {
        return {...state, fetching: true}
      }
      case "FETCH_SPORTS_NEWS_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "FETCH_SPORTS_NEWS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          sportsNews: action.payload,
        }
      }
    }

    return state
}
