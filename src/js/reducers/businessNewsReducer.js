export default function reducer(state={
    businessNews: [],
    fetching: false,
    fetched: false,
    error: null,
  }, action) {

    switch (action.type) {
      case "FETCH_NEWS": {
        return {...state, fetching: true}
      }
      case "FETCH_BUSINESS_NEWS_REJECTED": {
        return {...state, fetching: false, error: action.payload}
      }
      case "FETCH_BUSINESS_NEWS_FULFILLED": {
        return {
          ...state,
          fetching: false,
          fetched: true,
          businessNews: action.payload,
        }
      }
    }

    return state
}
