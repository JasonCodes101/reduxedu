import axios from "axios";

export function fetchNews() {
  return function(dispatch) {
    dispatch({type: "FETCH_NEWS"});

    /*
      http://rest.learncode.academy is a public test server, so another user's experimentation can break your tests
      If you get console errors due to bad data:
      - change "reacttest" below to any other username
      - post some NEWS to http://rest.learncode.academy/api/yourusername/NEWS
    */
    axios.get("https://newsapi.org/v1/articles?source=google-news&sortBy=top&apiKey=73387b48152c4b9c9a16a21baea4c65f")
      .then((response) => {
        dispatch({type: "FETCH_NEWS_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_NEWS_REJECTED", payload: err})
      })
  }
}


export function fetchSportsNews() {
  return function(dispatch) {
    dispatch({type: "FETCH_SPORTS_NEWS"});

    /*
      http://rest.learncode.academy is a public test server, so another user's experimentation can break your tests
      If you get console errors due to bad data:
      - change "reacttest" below to any other username
      - post some NEWS to http://rest.learncode.academy/api/yourusername/NEWS
    */
    axios.get("https://newsapi.org/v1/articles?source=espn&sortBy=top&apiKey=73387b48152c4b9c9a16a21baea4c65f")
      .then((response) => {
        dispatch({type: "FETCH_SPORTS_NEWS_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_SPORTS_NEWS_REJECTED", payload: err})
      })
  }
}
