import axios from "axios";

export function fetchBusinessNews() {
  return function(dispatch) {
    dispatch({type: "FETCH_NEWS"});

    /*
      http://rest.learncode.academy is a public test server, so another user's experimentation can break your tests
      If you get console errors due to bad data:
      - change "reacttest" below to any other username
      - post some NEWS to http://rest.learncode.academy/api/yourusername/NEWS
    */
    axios.get("https://newsapi.org/v1/articles?source=business-insider&sortBy=top&apiKey=73387b48152c4b9c9a16a21baea4c65f")
      .then((response) => {
        dispatch({type: "FETCH_BUSINESS_NEWS_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "FETCH_BUSINESS_NEWS_REJECTED", payload: err})
      })
  }
}
