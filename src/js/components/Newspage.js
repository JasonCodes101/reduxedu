import React from "react"
import { connect } from "react-redux"
import {Grid,Row,Col,Thumbnail,Button,Tabs,Tab} from "react-bootstrap";
import { fetchNews,fetchSportsNews } from "../actions/newsActions";
import { fetchBusinessNews } from "../actions/BusinessNews";
import ReactSpinner from 'reactjs-spinner';

@connect((store) => {
  return {
    news: store.news.news,
    sportsNews: store.sportsNews.sportsNews,
    businessNews: store.businessNews.businessNews
  };
})
export default class NewsPage extends React.Component {
  componentWillMount() {
    this.props.dispatch(fetchNews())
    this.props.dispatch(fetchSportsNews())
    this.props.dispatch(fetchBusinessNews())
  }
  //
  // fetchNews() {
  //   this.props.dispatch(fetchNews())
  // }

  handleSelect() {
  alert('selected');
  }


  loadNewsCards(data){
    return data.articles.map((value,k) =>{
      return(
        //<Grid >

          <Col xs={6} md={4} key={k}  >
            <Thumbnail src={value.urlToImage} alt="242x200">
              <h3>{value.title}</h3>
              <p>{value.description}</p>
              <p>
                <Button bsStyle="default" href={value.url}>Go to link</Button>
                {/* <Button bsStyle="primary" href={value.urlToImage}>View Image</Button>&nbsp; */}

              </p>
            </Thumbnail>
          </Col>



      )
    })
  }

  loadSportNewsCards(data){
    var card = {
      height:400
    }
    return data.articles.map((value,k) =>{
      return(
        //<Grid >

          <Col xs={6} md={4} key={k}  >
            <Thumbnail src={value.urlToImage} alt="242x200">
              <h3>{value.title}</h3>
              <p>{value.description}</p>
              <p>
                <Button bsStyle="default" href={value.url}>Go to link</Button>
                {/* <Button bsStyle="primary" href={value.urlToImage}>View Image</Button>&nbsp; */}

              </p>
            </Thumbnail>
          </Col>



      )
    })
  }

  loadBusinessNewsCards(data){
    return data.articles.map((value,k) =>{
      return(
        //<Grid >

          <Col xs={6} md={4} key={k}  >
            <Thumbnail src={value.urlToImage} alt="242x200">
              <h3>{value.title}</h3>
              <p>{value.description}</p>
              <p>
                <Button bsStyle="default" href={value.url}>Go to link</Button>
                {/* <Button bsStyle="primary" href={value.urlToImage}>View Image</Button>&nbsp; */}

              </p>
            </Thumbnail>
          </Col>



      )
    })
  }

  render() {
    const { news,sportsNews,businessNews } = this.props;
      if(!news.length  && news.length === 0 ){
        return(
          <div style={{backgroundColor:"black"}}>
            <ReactSpinner>Example</ReactSpinner>
          </div>
        )
      }else {
        return (
          <div >
            <Tabs defaultActiveKey={1} animation={false} id="noanim-tab-example">
              <Tab eventKey={1} title="All News">
                <Grid  fluid={true} >
                  <span>
                    <Row >
                      { this.loadNewsCards(news)}
                      { this.loadSportNewsCards(sportsNews)}
                      { this.loadBusinessNewsCards(businessNews)}
                    </Row>
                </span>
                </Grid>
              </Tab>
              <Tab eventKey={2} title="Sports News">
                <Grid  fluid={true} >
                  <span>
                    <Row >
                      { this.loadSportNewsCards(sportsNews)}
                    </Row>
                </span>
                </Grid>
              </Tab>
              <Tab eventKey={3} title="Business News">
                <Grid  fluid={true} >
                <span>
                  <Row >
                    { this.loadBusinessNewsCards(businessNews)}
                  </Row>
              </span>
              </Grid></Tab>
            </Tabs>
          <br/>
            <div>
            </div>

          </div>
        )
      }

  }
}
